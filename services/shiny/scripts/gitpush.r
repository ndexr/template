box::use(gert)
box::use(glue[glue])

curbranch <- gert$git_branch()
curbranch_split <- strsplit(curbranch, "-")[[1]]
new_branch <- paste0(curbranch_split[[1]], "-", as.numeric(curbranch_split[[2]]) + 1)
gert$git_commit_all(message = glue('Commiting {curbranch} by {system("whoami")} at {as.character(Sys.time())}'))
gert$git_push(verbose = TRUE)
gert$git_branch_create(new_branch, checkout = TRUE)
