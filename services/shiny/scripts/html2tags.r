# html2R::html2R()

#' @export
includeNode <- function(node) {
  box::use(. / html2tags)
  !html2tags$isSep(node) && !html2tags$isComment(node)
}


#' @export
isComment <- function(node) {
  identical(names(node), c("type", "content")) && node[["type"]] ==
    "comment"
}

#' @export
isSep <- function(node) {
  identical(names(node), c("type", "content")) && node[["type"]] ==
    "text" && grepl("^\\s*?$", node[["content"]])
}

#' @export
outdent <- function(content) {
  if (grepl("^\\s*?$", content)) {
    return("")
  }
  lines <- strsplit(gsub("(^\n| *$)", "", content), "\n")[[1L]]
  i <- 1L
  while (i <= length(lines) && grepl("^\\s*?$", lines[i])) {
    i <- i + 1L
  }
  if (i > 1L) {
    lines <- lines[-seq_len(i - 1L)]
  }
  space <- min(nchar(sub("(^\\s*).*", "\\1", Filter(function(x) {
    x !=
      ""
  }, lines))))
  sub(sprintf("^\\s{%d}", space), "", lines)
}

#' @export
parse_attribute <- function(attr) {
  as.character(glue("{key} = \"{value}\"", key = ifelse(attr$key ==
    "for" || grepl("-", attr$key) || grepl(":", attr$key),
  sprintf("`%s`", attr$key), attr$key
  ), value = ifelse(length(attr$value),
    sprintf("%s", attr$value), ""
  )))
}

#' @export
parse_content <- function(node, html) {
  box::use(stats)
  box::use(. / html2tags)
  code <- if (html) {
    gsub("\n", "\n  ", paste0(stats$na.omit(c(
      vapply(node[["attributes"]],
        html2tags$parse_attribute, character(1L),
        USE.NAMES = FALSE
      ),
      if (length(node[["children"]])) {
        formattedContent <- html2tags$outdent(gsub(
          "\"", "\\\\\"",
          node[["children"]][[1L]][["content"]]
        ))
        ifelse(identical(formattedContent, ""), NA_character_,
          sprintf("HTML(\n%s\n)", paste0(
            c(
              "  \"\\n\"",
              sprintf("  \"%s\\n\"", formattedContent)
            ),
            collapse = ",\n"
          ))
        )
      }
    )), collapse = ",\n"))
  } else {
    gsub("\n", "\n  ", paste0(c(
      vapply(node[["attributes"]],
        html2tags$parse_attribute, character(1L),
        USE.NAMES = FALSE
      ),
      vapply(Filter(html2tags$includeNode, node[["children"]]), html2tags$parse_node,
        character(1L),
        USE.NAMES = FALSE
      )
    ), collapse = ",\n"))
  }
  if (grepl("^\\s*?$", code)) {
    ""
  } else {
    sprintf("\n  %s\n", code)
  }
}

#' @export
parse_html <- function(html) {
  box::use(. / html2tags)
  paste0(Filter(function(x) x != "", vapply(
    html, html2tags$parse_node,
    character(1L)
  )), collapse = ",\n")
}

#' @export
parse_node <- function(node) {
  box::use(. / html2tags)
  box::use(glue[glue])
  code <- ""
  if (node[["type"]] == "element") {
    if (node[["tagName"]] == "!doctype") {
      return("")
    }
    if (node[["tagName"]] == "br") {
      return("tags$br()")
    }
    html <- node[["tagName"]] %in% c("script", "style")
    code <- glue("tags${tag}({content})",
      tag = node[["tagName"]],
      content = html2tags$parse_content(node, html)
    )
  } else if (node[["type"]] == "text" && !html2tags$isSep(node)) {
    code <- glue("\"{content}\"", content = gsub(
      "\\\\",
      "\\\\\\\\", gsub("(^\\s*|\\s*$)", "", node[["content"]])
    ))
  }
  as.character(code)
}



#' @export
ui <- function() {
  box::use(shiny = shiny[tags])
  # box::use(jqui)
  box::use(shinyAce)
  shiny$fluidPage(
    shiny$fileInput("file", label = NULL, buttonLabel = "Upload HTML file..."),
    shiny$actionButton("prettify", "Prettify", class = "btn-danger btn-lg"),
    shiny$actionButton("parse", "Convert",
      class = "btn-danger btn-lg"
    ),
    shiny$actionButton("copy", "Copy to clipboard",
      class = "btn-danger btn-lg"
    ),
    shinyAce$aceEditor("aceHTML",
      value = "",
      mode = "html",
      height = "calc(100vh - 169px - 10px)", tabSize = 2,
      placeholder = "Paste some HTML code here or upload a HTML file."
    ),
    shinyAce$aceEditor("aceR",
      value = "", mode = "r",
      height = "calc(100vh - 169px - 10px)",
      tabSize = 2
    )
  )
}

#' @export
server <- function(input, output, session) {
  {
    box::use(shiny = shiny[tags])
    # box::use(jqui)
    box::use(shinyAce)
  }

  shiny$observeEvent(input[["file"]],
    {
      shinyAce$updateshinyAce$aceEditor(session, "aceHTML", value = paste0(suppressWarnings(readLines(input[["file"]][["datapath"]])),
        collapse = "\n"
      ))
      shinyAce$updateshinyAce$aceEditor(session, "aceR", value = "")
    },
    priority = 2
  )

  shiny$observeEvent(input[["file"]],
    {
      session$sendCustomMessage("updateScrollBarH", "HTML")
    },
    priority = 1
  )

  shiny$observeEvent(input[["json"]],
    {
      shinyAce$updateshinyAce$aceEditor(session, "aceR", value = html2tags$parse_html(input[["json"]]))
    },
    priority = 2
  )

  shiny$observeEvent(input[["json"]],
    {
      session$sendCustomMessage("updateScrollBarH", "R")
    },
    priority = 1
  )
}


if (FALSE) {
  box::use(. / html2tags)
  box::reload(html2tags)
}
