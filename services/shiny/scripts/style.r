exclude_dirs <- fs::dir_ls(type = "directory", regexp = "(.*?)renv", recurse = T)
styler::style_dir(".", exclude_dirs = exclude_dirs)
