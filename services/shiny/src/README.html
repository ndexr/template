<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en"><head>

<meta charset="utf-8">
<meta name="generator" content="quarto-1.2.335">

<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">


<title>Deploying Shiny Applications</title>
<style>
code{white-space: pre-wrap;}
span.smallcaps{font-variant: small-caps;}
div.columns{display: flex; gap: min(4vw, 1.5em);}
div.column{flex: auto; overflow-x: auto;}
div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
ul.task-list{list-style: none;}
ul.task-list li input[type="checkbox"] {
  width: 0.8em;
  margin: 0 0.8em 0.2em -1.6em;
  vertical-align: middle;
}
</style>


<script src="README_files/libs/clipboard/clipboard.min.js"></script>
<script src="README_files/libs/quarto-html/quarto.js"></script>
<script src="README_files/libs/quarto-html/popper.min.js"></script>
<script src="README_files/libs/quarto-html/tippy.umd.min.js"></script>
<script src="README_files/libs/quarto-html/anchor.min.js"></script>
<link href="README_files/libs/quarto-html/tippy.css" rel="stylesheet">
<link href="README_files/libs/quarto-html/quarto-syntax-highlighting.css" rel="stylesheet" id="quarto-text-highlighting-styles">
<script src="README_files/libs/bootstrap/bootstrap.min.js"></script>
<link href="README_files/libs/bootstrap/bootstrap-icons.css" rel="stylesheet">
<link href="README_files/libs/bootstrap/bootstrap.min.css" rel="stylesheet" id="quarto-bootstrap" data-mode="light">


</head>

<body class="fullcontent">

<div id="quarto-content" class="page-columns page-rows-contents page-layout-article">

<main class="content" id="quarto-document-content">

<header id="title-block-header" class="quarto-title-block default">
<div class="quarto-title">
<h1 class="title">Deploying Shiny Applications</h1>
</div>



<div class="quarto-title-meta">

    
  
    
  </div>
  

</header>

<section id="introduction" class="level2">
<h2 class="anchored" data-anchor-id="introduction">Introduction</h2>
<p>Deploying Shiny applications to the cloud has become necessary for businesses. However, it’s time-consuming and complex; Docker and GitLab have become popular tools that help simplify this process. This document serves as an opinionated way of using them to deploy.</p>
<p>We will use Docker Compose and GitLab CI to deploy an application to Amazon Elastic Compute Cloud (EC2). Docker Compose is a tool for defining and running multi-container Docker applications. GitLab CI is a continuous integration and deployment tool that automates the process of building, testing, and deploying applications. Docker Compose launches the application itself, and GitLab is the glue between the application and your end user.</p>
<p>We will begin by discussing the basics of Docker and Docker Compose, including how to create <code>Dockerfile</code> and <code>docker-compose.yaml</code> file. We will then move on to GitLab CI, exploring how to set up a pipeline and generate a <code>.gitlab-ci.yml</code> file that defines the stages and jobs needed for deployment. Then, we will dive into deploying on EC2, configure security groups, and connect to the instance using SSH.</p>
</section>
<section id="quick-start" class="level2">
<h2 class="anchored" data-anchor-id="quick-start">Quick Start</h2>
<ol type="1">
<li>Create ssh key using <code>ssh-keygen</code> on server, and pass <code>~/.ssh/id_rsa.pub</code> to GitLab.</li>
<li>Clone the repository <code>git clone git@gitlab.com:ndexr/template.git</code></li>
</ol>
<section id="rebuilding-the-local-dev-environment" class="level3">
<h3 class="anchored" data-anchor-id="rebuilding-the-local-dev-environment">Rebuilding The Local Dev Environment</h3>
<ol type="1">
<li>Go to RStudio and Create a New Project From Existing Directory
<ul>
<li>Use <code>./services/shiny</code></li>
</ul></li>
<li>Execute <code>renv::restore()</code> in the RStudio Console</li>
<li>Execute <code>npm i</code> in <code>./servces/shiny</code> to recreate the <code>node_modules</code> folder</li>
<li>Open <code>app.r</code> and source</li>
</ol>
</section>
<section id="building-containers-locally" class="level3">
<h3 class="anchored" data-anchor-id="building-containers-locally">Building Containers Locally</h3>
<ol type="1">
<li>Execute <code>make local</code> in <code>./</code></li>
</ol>
</section>
</section>
<section id="what-is-amazon-elastic-compute-cloud" class="level2">
<h2 class="anchored" data-anchor-id="what-is-amazon-elastic-compute-cloud">What is Amazon Elastic Compute Cloud?</h2>
<p>Amazon Elastic Compute Cloud (Amazon EC2) is a web service that provides resizable computing capacity. You rent virtual machines (VMs or instances) on which your applications are served to the customer. These VMs can be resized for additional cores, memory, and disk space.</p>
<p>And it’s a good choice for deploying Shiny applications. You get a scalable and flexible infrastructure that lets you quickly and easily spin up or down. You can easily handle varying traffic loads and adjust your computing resources. Amazon EC2 also offers a wide range of security features, such as firewalls, encryption, and secure access controls, which can help harden your apps for the real world.</p>
<section id="ports-and-ip-addresses" class="level3">
<h3 class="anchored" data-anchor-id="ports-and-ip-addresses">Ports and IP Addresses</h3>
<p>IP addresses are used to identify and locate devices on a network and play a critical role in enabling communication between devices on the internet. An IP address consists of numbers separated by dots, such as <code>192.168.0.1</code>.</p>
<p>IP addresses can be either static or dynamic. Static IP addresses remain the same over time, while dynamic IP addresses are assigned to devices each time they connect to the network and may change over time.</p>
</section>
<section id="elastic-ips-make-accessing-your-server-easier." class="level3">
<h3 class="anchored" data-anchor-id="elastic-ips-make-accessing-your-server-easier.">Elastic IPs make accessing your server easier.</h3>
<p>An Elastic IP address (EIP) is a static, public IPv4 address that can be dynamically allocated to an EC2 server. Elastic IP addresses are useful when you associate a static IP address with a resource that may be stopped, started, or replaced. Using an EIP, you maintain a consistent IP address associated with the resource.</p>
<p>EIPs incur charges when they are not associated with a running instance or when they are associated with an instance that is stopped or terminated. It’s recommended to release the EIP when it’s not in use to avoid incurring unnecessary charges.</p>
</section>
<section id="put-your-domain-behind-an-elastic-ip" class="level3">
<h3 class="anchored" data-anchor-id="put-your-domain-behind-an-elastic-ip">Put Your Domain behind an Elastic IP</h3>
<p>When you use a domain name to access a resource, the DNS system translates the domain name into an IP address that identifies the resource. If the IP address changes, the DNS system needs to be updated with the new IP address. This process can take time and may result in temporary downtime for your application or website.</p>
<p>Using an EIP with your domain name can avoid these issues because the EIP remains constant even if the underlying resource changes. This means that you can associate the domain name with the EIP and then associate the EIP with the resource, ensuring that the domain name always points to the correct IP address.</p>
</section>
<section id="ports-are-windows-into-your-server-security-groups-help-you-manage-them." class="level3">
<h3 class="anchored" data-anchor-id="ports-are-windows-into-your-server-security-groups-help-you-manage-them.">Ports are windows into your server; security groups help you manage them.</h3>
<p>A port is a communication endpoint that allows network services to listen for incoming connections from other devices over the internet. Each application running on an EC2 instance is typically associated with a specific port number. By default, EC2 instances are launched with all ports closed, meaning no incoming traffic can access the instance. We need to create a security group to allow incoming traffic on specific ports.</p>
<p>A security group acts as a virtual firewall that controls the traffic into or out of an EC2 instance. At BioMarin, in our qualified environment, I cannot log into RStudio Server and then clone a repository from GitHub, only the company’s private GitLab server. In this instance, <em>outbound</em> traffic is blocked, and from the command line, some servers exist in the world that I cannot access from <em>inside</em> the computer. However, I can access RStudio Server running on that server. In this instance, <em>incoming</em> traffic on port 8787 is open, and from <em>outside</em> of the server, I am able to gain access to whatever is accessible to me through the RStudio Server application.</p>
<p>In the EC2 Console, you can configure a security group to specify a set of rules surrounding ports on your server and from which IP addresses those ports are accessible. This means that you can use one server to deploy your application, allowing anyone in the world to see your application - while also preventing anyone but yourself from accessing RStudio Server running on that same EC2 server because you can whitelist your home’s IP address as the only location from which that port is accessible. This is pretty cool because it gives you much flexibility in managing your applications in one location without sacrificing security. Here is a table of common ports used for various applications, along with whether or not it is common to be open to the world and the IP address to use for each application:</p>
<table class="table">
<colgroup>
<col style="width: 18%">
<col style="width: 18%">
<col style="width: 18%">
<col style="width: 27%">
<col style="width: 18%">
</colgroup>
<thead>
<tr class="header">
<th>Application</th>
<th>Port</th>
<th>Open to world</th>
<th>IP address</th>
<th>Example</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>HTTP</td>
<td>80</td>
<td>Yes</td>
<td>0.0.0.0/0</td>
<td>Anyone in World</td>
</tr>
<tr class="even">
<td>HTTPS</td>
<td>443</td>
<td>Yes</td>
<td>0.0.0.0/0</td>
<td>Anyone in World</td>
</tr>
<tr class="odd">
<td>SSH</td>
<td>22</td>
<td>No</td>
<td>Specific IP address or range</td>
<td>Employee Home IPs</td>
</tr>
<tr class="even">
<td>RStudio Server</td>
<td>8787</td>
<td>No</td>
<td>Specific IP address or range</td>
<td>Employee Home IPs</td>
</tr>
<tr class="odd">
<td>VSCode</td>
<td>8080</td>
<td>No</td>
<td>Specific IP address or range</td>
<td>Employee Home IPs</td>
</tr>
<tr class="even">
<td>PostgreSQL</td>
<td>5432</td>
<td>No</td>
<td>Specific IP address or range</td>
<td>App Host IP</td>
</tr>
</tbody>
</table>
<p>It’s important to note that whether or not to open a port to the world depends on the specific use case and security requirements. In general, it’s recommended to only open ports to the world necessary for the application to function and to restrict access to specific IP addresses or ranges whenever possible. However, in practice - if you are playing around on an EC2 server and not worried much about whether or not something happens to it, open them all up and play around.</p>
</section>
</section>
<section id="docker-compose" class="level2">
<h2 class="anchored" data-anchor-id="docker-compose">Docker Compose</h2>
<section id="docker-compose-is-generally-sufficient-and-provides-easy-ways-to-scale" class="level3">
<h3 class="anchored" data-anchor-id="docker-compose-is-generally-sufficient-and-provides-easy-ways-to-scale">Docker Compose is generally sufficient and provides easy ways to scale</h3>
<p>Docker Compose is a tool for defining and running multi-container Docker applications. It allows you to define related services, such as web servers, databases, and messaging queues, and run them together as a single application.</p>
<p>With Docker Compose, you can define your application’s infrastructure as code in a YAML file, which makes it easy to share and version control your application’s configuration. Docker Compose also lets you specify dependencies and relationships between services and configure each service’s environment variables and network settings.</p>
<p>Once you have defined your Docker Compose configuration, you can use the <code>docker-compose</code> command-line tool to start, stop, and manage your application. Docker Compose handles the creation and management of the containers, networks, and volumes needed for your application to run.</p>
</section>
<section id="docker-compose.yaml" class="level3">
<h3 class="anchored" data-anchor-id="docker-compose.yaml"><code>docker-compose.yaml</code></h3>
<p>This <code>docker-compose.yaml</code> file defines a single service called <code>shiny</code>, which will build and run a Docker container for a Shiny web application. For now, we will only focus on the <code>shiny</code> service except for one point - and that is to say that when this application is deployed if you wanted to access the Redis database from your local computer, you would use <code>http://&lt;server-ip&gt;:6379</code> but the Shiny application can access it internally without exposing a port to the world using <code>http://redis:6379</code>.</p>
<pre><code>version: '3.9'
services:
  shiny:
    container_name: shiny
    restart: unless-stopped
    build:
      context: ./services/shiny
    ports:
      - 9010:8000
    network_mode: bridge
    command: ["R", "-e", "shiny::runApp('/app/src')"]
    profiles:
      - local
  redis:
    container_name: ndexr_redis
    restart: unless-stopped
    image: redis:6.2-alpine
    command: redis-server
    volumes:
      - /srv/db/redis:/data
    ports:
      - '6379:6379'
    expose:
      - '6379'
    network_mode: bridge
    profiles:
      - local</code></pre>
<p>Here’s what each of the lines does:</p>
<ul>
<li><p><code>version: '3.9'</code>: This specifies the version of the Docker Compose file format to use.</p></li>
<li><p><code>services</code>: This section lists the various services to be run as part of the Docker Compose configuration.</p></li>
<li><p><code>shiny</code>: This is the name of the service that we’re defining.</p></li>
<li><p><code>container_name: shiny</code>: This sets the name of the Docker container created for this service to “shiny”</p></li>
<li><p><code>restart: unless-stopped</code>: This tells Docker to automatically restart the container if it stops for any reason unless the user explicitly stops it.</p>
<ul>
<li>This can be set to <code>no</code> to disable automatic container restarts or to <code>always</code> to restart the container, even if it exits with a zero status code.</li>
</ul></li>
<li><p><code>build</code>: This section specifies how to build the Docker image for the service. In this case, it’s using a <code>context</code> of <code>./services/shiny</code>, which means it will look for a <code>Dockerfile</code> in that directory and use it to build the image. This is where the shiny application lives in this repository. This is because a shiny application is a smaller subset of a deployed application.</p></li>
<li><p><code>ports</code>: This specifies how to map ports between the host machine and the container. In this case, it’s mapping port 9010 on the host to port 8000 in the container. This is a <em>very important</em> concept.</p>
<ul>
<li><p>In the Docker Compose network, the hostname and port for the shiny application is specified as <code>shiny:8000</code>. This means that other services can access the shiny application within the Docker network using that hostname and port combination.</p>
<p>However, if you want to access the shiny application from outside the Docker network (e.g.&nbsp;from the server itself or another machine on the same network), you need to use the IP address and port of the Docker host.</p>
<p>My home server’s IP address is, and the port is <code>9000</code>. So to access the shiny application from the server itself, you would need to use the URL <code>http://192.168.0.68:9000</code>. When I use this URL, the request will be routed to the Docker host, which will then forward the request to the <code>shiny</code> service running in the Docker container on port <code>8000</code>.</p>
<ul>
<li><code>http://192.168.0.68:9000</code> maps to <code>shiny:8000</code></li>
</ul></li>
</ul></li>
<li><p><code>network_mode: bridge</code>: This sets the network mode for the container to “bridge”, which means it will use the default Docker network and IP address.</p></li>
<li><p><code>command: ["R", "-e", "shiny::runApp('/app/src')]</code>: This specifies the command to run when the container starts up. In this case, it’s starting an R process and running a Shiny app from the <code>/app/src</code> directory.</p></li>
<li><p><code>profiles: - local</code>: This specifies the Docker profile that this service is associated with. Profiles are a way to group and manage multiple configurations for the same Compose file, and they can be used to specify different configuration options based on the environment (e.g.&nbsp;development, production, etc.).</p></li>
</ul>
<p>Here are some examples of other options that can be used in a Docker Compose file:</p>
<ul>
<li><p><code>volumes</code>: These can mount directories or files from the host machine into the container. This can be useful for sharing data between the host and the container.</p></li>
<li><p><code>environment</code>: This can be used to set environment variables in the container. This can be useful for configuring the application or passing sensitive information like passwords.</p></li>
</ul>
</section>
</section>
<section id="gitlab-ci" class="level2">
<h2 class="anchored" data-anchor-id="gitlab-ci">GitLab CI</h2>
<p>GitLab CI (Continuous Integration) is a part of GitLab, a web-based Git repository manager, which provides a continuous integration and deployment pipeline for software development projects. It allows developers to automate the building, testing, and deployment of code changes by defining a set of rules in a configuration file called <code>.gitlab-ci.yml</code>. GitLab CI is useful because it simplifies the software development process, reduces the chances of errors, and speeds up the overall software development process.</p>
<section id="gitlab-runner" class="level3">
<h3 class="anchored" data-anchor-id="gitlab-runner">Gitlab Runner</h3>
<p>GitLab Runner is an open-source tool that runs jobs and executes tasks in a GitLab CI/CD pipeline. GitLab Runner can be installed on different platforms, including on-premises servers, cloud-based instances, and EC2 instances. Here’s how GitLab Runner works on EC2:</p>
<ol type="1">
<li><p>First, you must launch an EC2 instance and install GitLab Runner. This can be done by following the official GitLab Runner installation guide for Linux; however, the Runners installation is already automated.</p></li>
<li><p>Once GitLab Runner is installed, you must register the EC2 instance as a Runner with your GitLab instance. This involves generating a token on the GitLab server and then using it to register the Runner on the EC2 instance.</p></li>
<li><p>After registering the Runner, you can configure it to run jobs and execute tasks in your GitLab CI/CD pipeline. This is done by creating a <code>.gitlab-ci.yml</code> file in your GitLab project’s repository, which defines the pipeline’s stages, jobs, and commands.</p></li>
<li><p>When a commit is made to the repository, GitLab triggers the pipeline, which sends the job to the registered Runner on the EC2 instance. The Runner then executes the job and reports the results back to GitLab.</p></li>
<li><p>Once the job is completed, the EC2 instance can be terminated or kept running for future jobs, depending on your configuration. In the case of our deployed applications, GitLab Runner runs on the EC2 server and launches the application on that same server.</p></li>
</ol>
</section>
<section id="gitlab-ci.yml" class="level3">
<h3 class="anchored" data-anchor-id="gitlab-ci.yml"><code>.gitlab-ci.yml</code></h3>
<p>The <code>.gitlab-ci.yml</code> file is a configuration file for GitLab CI/CD that defines the pipeline stages and jobs that should be executed when code is pushed to a GitLab repository.</p>
<pre><code>stages:
  - production

deploy:
  stage: production
  only:
    - main
  environment: production
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  script:
    - docker compose --profile=local pull --ignore-pull-failures
    - docker compose --profile=local build
    - docker compose --profile=local push
    - docker compose --profile=local up -d --remove-orphans</code></pre>
<p>The <code>deploy</code> job is set up to run only when the code is pushed to the <code>main</code> branch. When it runs, it uses the <code>docker:latest</code> image as its base image and starts a <code>docker:dind</code> service (Docker in Docker) to enable the deployment of Docker images within the GitLab CI/CD pipeline.</p>
<p>Before running the main script, the <code>before_script</code> section is executed. In this section, the <code>docker login</code> command is used to authenticate with the Docker registry using the credentials stored as GitLab CI/CD variables (<code>$CI_REGISTRY_USER</code> and <code>$CI_REGISTRY_PASSWORD</code>). This is necessary to be able to push and pull Docker images to the registry.</p>
<p>The <code>script</code> section contains the commands that the job will execute. In this case, the job uses the <code>docker compose</code> command to manage the deployment of the application defined in the <code>docker-compose.yml</code> file. The <code>--profile=local</code> option specifies that the <code>local</code> profile defined in the <code>docker-compose.yml</code> file should be used.</p>
<p>The script performs the following actions in order:</p>
<ol type="1">
<li><p><code>docker compose --profile=local pull --ignore-pull-failures</code>: Pulls the latest version of the Docker images defined in the <code>docker-compose.yml</code> file from the Docker registry, ignoring any failures that occur during the pull process.</p></li>
<li><p><code>docker compose --profile=local build</code>: Builds the Docker images defined in the <code>docker-compose.yml</code> file.</p></li>
<li><p><code>docker compose --profile=local push</code>: Pushes the Docker images defined in the <code>docker-compose.yml</code> file to the Docker container registry.</p></li>
<li><p><code>docker compose --profile=local up -d --remove-orphans</code>: Starts the Docker containers defined in the <code>docker-compose.yml</code> file, using the <code>local</code> profile, in detached mode (<code>-d</code>). The <code>--remove-orphans</code> option removes any previously created containers that are no longer defined in the <code>docker-compose.yml</code> file.</p></li>
</ol>
<p>The <code>deploy</code> job in the <code>.gitlab-ci.yml</code> file works together with the <code>docker-compose.yml</code> file to automate the deployment of a Shiny application. The <code>docker-compose.yml</code> file defines the containers, networks, and volumes required to run the application, while the <code>deploy</code> job in the <code>.gitlab-ci.yml</code> file pulls, builds, and pushes the Docker images defined in the <code>docker-compose.yml</code> file to a Docker registry and then starts the containers in the production environment on the EC2 server.</p>
</section>
<section id="makefile-for-local-development" class="level3">
<h3 class="anchored" data-anchor-id="makefile-for-local-development"><code>Makefile</code> for Local Development</h3>
<pre><code>local:
    docker compose --profile=local build
    docker compose --profile=local up -d --remove-orphans
    docker compose --profile=local logs -f</code></pre>
<p>This Makefile is a set of instructions that automate the local development process of the application, similar to what is done by the <code>deploy</code> job in the <code>.gitlab-ci.yaml</code> file for production. If you are building the application locally, this is a convenient way to launch the application.</p>
<p>The <code>local</code> target in the Makefile has three commands that execute one after the other, the only new command is:</p>
<ul>
<li><code>docker compose --profile=local logs -f</code>: This command displays the logs for the running containers in a continuous stream (<code>-f</code>).</li>
</ul>
<p>By running <code>make local</code> you can get up and running quickly.</p>
</section>
</section>
<section id="structuring-shiny-applications" class="level2">
<h2 class="anchored" data-anchor-id="structuring-shiny-applications">Structuring Shiny Applications</h2>
<p>The shiny service has <code>Dockerfile</code> located in <code>./services/shiny</code> and is used to build the Docker image for the shiny service in the <code>docker-compose.yaml</code> file. It starts with a base image that has R, and then it installs additional system packages that R apps tend to need. The image installs the <code>renv</code> package management in R and Python packages through <code>conda</code>. It sets up the working directory <code>/app</code> and then copies the <code>renv.lock</code> file, <code>onload</code> directory, the R script, and <code>src</code> folder into that directory. The <code>package.json</code> file specifies the libraries from <code>npm</code> we use and their versions. The <code>renv.lock</code>, <code>requirements.txt</code>, and <code>package.json</code> assist with recreating the R, python, and npm environments used in developing the shiny app.</p>
<section id="section" class="level3">
<h3 class="anchored" data-anchor-id="section"></h3>
</section>
</section>

</main>
<!-- /main column -->
<script id="quarto-html-after-body" type="application/javascript">
window.document.addEventListener("DOMContentLoaded", function (event) {
  const toggleBodyColorMode = (bsSheetEl) => {
    const mode = bsSheetEl.getAttribute("data-mode");
    const bodyEl = window.document.querySelector("body");
    if (mode === "dark") {
      bodyEl.classList.add("quarto-dark");
      bodyEl.classList.remove("quarto-light");
    } else {
      bodyEl.classList.add("quarto-light");
      bodyEl.classList.remove("quarto-dark");
    }
  }
  const toggleBodyColorPrimary = () => {
    const bsSheetEl = window.document.querySelector("link#quarto-bootstrap");
    if (bsSheetEl) {
      toggleBodyColorMode(bsSheetEl);
    }
  }
  toggleBodyColorPrimary();  
  const icon = "";
  const anchorJS = new window.AnchorJS();
  anchorJS.options = {
    placement: 'right',
    icon: icon
  };
  anchorJS.add('.anchored');
  const clipboard = new window.ClipboardJS('.code-copy-button', {
    target: function(trigger) {
      return trigger.previousElementSibling;
    }
  });
  clipboard.on('success', function(e) {
    // button target
    const button = e.trigger;
    // don't keep focus
    button.blur();
    // flash "checked"
    button.classList.add('code-copy-button-checked');
    var currentTitle = button.getAttribute("title");
    button.setAttribute("title", "Copied!");
    let tooltip;
    if (window.bootstrap) {
      button.setAttribute("data-bs-toggle", "tooltip");
      button.setAttribute("data-bs-placement", "left");
      button.setAttribute("data-bs-title", "Copied!");
      tooltip = new bootstrap.Tooltip(button, 
        { trigger: "manual", 
          customClass: "code-copy-button-tooltip",
          offset: [0, -8]});
      tooltip.show();    
    }
    setTimeout(function() {
      if (tooltip) {
        tooltip.hide();
        button.removeAttribute("data-bs-title");
        button.removeAttribute("data-bs-toggle");
        button.removeAttribute("data-bs-placement");
      }
      button.setAttribute("title", currentTitle);
      button.classList.remove('code-copy-button-checked');
    }, 1000);
    // clear code selection
    e.clearSelection();
  });
  function tippyHover(el, contentFn) {
    const config = {
      allowHTML: true,
      content: contentFn,
      maxWidth: 500,
      delay: 100,
      arrow: false,
      appendTo: function(el) {
          return el.parentElement;
      },
      interactive: true,
      interactiveBorder: 10,
      theme: 'quarto',
      placement: 'bottom-start'
    };
    window.tippy(el, config); 
  }
  const noterefs = window.document.querySelectorAll('a[role="doc-noteref"]');
  for (var i=0; i<noterefs.length; i++) {
    const ref = noterefs[i];
    tippyHover(ref, function() {
      // use id or data attribute instead here
      let href = ref.getAttribute('data-footnote-href') || ref.getAttribute('href');
      try { href = new URL(href).hash; } catch {}
      const id = href.replace(/^#\/?/, "");
      const note = window.document.getElementById(id);
      return note.innerHTML;
    });
  }
  const findCites = (el) => {
    const parentEl = el.parentElement;
    if (parentEl) {
      const cites = parentEl.dataset.cites;
      if (cites) {
        return {
          el,
          cites: cites.split(' ')
        };
      } else {
        return findCites(el.parentElement)
      }
    } else {
      return undefined;
    }
  };
  var bibliorefs = window.document.querySelectorAll('a[role="doc-biblioref"]');
  for (var i=0; i<bibliorefs.length; i++) {
    const ref = bibliorefs[i];
    const citeInfo = findCites(ref);
    if (citeInfo) {
      tippyHover(citeInfo.el, function() {
        var popup = window.document.createElement('div');
        citeInfo.cites.forEach(function(cite) {
          var citeDiv = window.document.createElement('div');
          citeDiv.classList.add('hanging-indent');
          citeDiv.classList.add('csl-entry');
          var biblioDiv = window.document.getElementById('ref-' + cite);
          if (biblioDiv) {
            citeDiv.innerHTML = biblioDiv.innerHTML;
          }
          popup.appendChild(citeDiv);
        });
        return popup.innerHTML;
      });
    }
  }
});
</script>
</div> <!-- /content -->



</body></html>