#' @export
ui_sidebar <- function(id = "sidebar") {
  box::use(shiny = shiny[tags, icon])
  box::use(shiny.router[route_link])
  box::use(. / sidebar)
  ns <- shiny$NS(id)
  out <- tags$div(
    tags$nav(
      class = "",
      tags$div(
        class = "row",
        tags$div(
          class = "col-12 p-1 d-flex justify-content-center align-items-center",
          tags$a(
            href = route_link(tolower("home")),
            class = "hoverable nav-link   p-1 m-3",
            title = "home",
            `data-bs-toggle` = "tooltip",
            `data-bs-placement` = "right",
            `data-bs-original` = "aws",
            tags$i(
              class = "bi bi-house-fill fa-2x"
            )
          ),
          tags$a(
            href = route_link(tolower("aws")),
            class = "hoverable nav-link   p-1 m-3",
            title = "cpu",
            `data-bs-toggle` = "tooltip",
            `data-bs-placement` = "right",
            `data-bs-original` = "aws",
            tags$i(
              class = "bi bi-cpu fa-2x"
            )
          ),
          tags$a(
            href = route_link(tolower("user_login")),
            class = "hoverable nav-link   p-1 m-3",
            title = "user_login",
            `data-bs-toggle` = "tooltip",
            `data-bs-placement` = "right",
            `data-bs-original` = "login",
            tags$i(
              class = "bi bi-power fa-2x"
            )
          )
        )
      )
    ),
    shiny$includeScript("javascript/sidebar.js")
  )

  out
}

#' @export
server_sidebar <- function(id) {
  box::use(shiny)
  box::use(shinyjs = shinyjs[js])
  shiny$moduleServer(
    id,
    function(input, output, session) {
      shiny$observeEvent(input$bgRandom, {
        js$bgRandom()
      })
    }
  )
}
