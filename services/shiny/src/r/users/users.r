#' @export
users_table <- function() {
  {
    box::use(.. / connections / postgres)
    box::use(DBI)
    box::use(uuid)
  }

  con <- postgres$connection_postgres()
  tbl_exists <- "console_users" %in% DBI$dbListTables(con)
  if (!tbl_exists) {
    console_users <- data.frame(email = uuid$UUIDgenerate(), password = uuid$UUIDgenerate())
    DBI$dbCreateTable(con, "console_users", console_users)
  }
  DBI$dbReadTable(con, "console_users")
}


#' @export
waitlist_table <- function(name, email, phone, company) {
  {
    box::use(.. / connections / postgres)
    box::use(DBI)
    box::use(uuid)
  }

  con <- postgres$connection_postgres()
  tbl_exists <- "waitlist" %in% DBI$dbListTables(con)
  waitlist <- data.frame(name = name, email = email, phone = phone, company = company)
  if (!tbl_exists) {
    DBI$dbCreateTable(con, "waitlist", waitlist)
  }
  DBI$dbAppendTable(con, "waitlist", waitlist)
}



#' @export
users_add <- function(email, password) {
  {
    box::use(.. / connections / postgres)
    box::use(DBI)
  }

  console_users <- data.frame(email = email, password = password)

  con <- postgres$connection_postgres()
  DBI$dbAppendTable(con, "console_users", console_users)
}

#' @export
ui_users <- function(id = "users") {
  box::use(shiny = shiny[tags])
  box::use
  ns <- shiny$NS(id)
  shiny$uiOutput(ns("ui"))
}

#' @export
server_users <- function(id = "users") {
  box::use(shiny = shiny[tags])
  shiny$moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      output$ui <- shiny$renderUI({
        shiny$div("users")
      })
    }
  )
}
